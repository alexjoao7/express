var express = require('express');
var jwt = require('jsonwebtoken');
var config = require('../configs/confij');
var protect = express.Router();

protect.use((req, res, next) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, config.key, (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Wrong token' });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        res.send({
            mensaje: 'Token not provided.'
        });
    }
});

module.exports = protect;