var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var config = require('../configs/confij');
var router = express.Router();

router.post('/', (req, res) => {
    if (req.header('user') === "test" && req.header('password') === "test") {
        const payload = {
            check: true
        };
        const token = jwt.sign(payload, config.key, {
            expiresIn: 1440
        });
        res.json({
            message: 'Correct Authentication',
            token: token
        });
    } else {
        res.json({ message: "Incorrect username or password" })
    }
})

module.exports = router;