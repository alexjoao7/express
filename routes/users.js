var express = require('express');
var router = express.Router();
var protected = require("../middleware/middleware");
var dbConn = require('../configs/bdd');

/* GET users listing. */
router.get('/', protected, function (req, res, next) {
  var form_data = {
    use_first_name: "Alex",
    use_last_name: "Merino",
    use_email: "alexmerino67@gmail.com",
    use_username: "alex",
    use_password: "abc123",
    use_dob: "Y",
    use_gender: "Male"
  }
  res.json(form_data);
});

// Post to save data
router.post('/add', protected, function (req, res, next) {

  var form_data = {
    use_first_name: req.body.first_name,
    use_last_name: req.body.last_name,
    use_email: req.body.email,
    use_username: req.body.username,
    use_password: req.body.password,
    use_dob: req.body.dob,
    use_gender: req.body.gender
  }

  // insert query
  dbConn.query('INSERT INTO user SET ?', form_data, function (err, result) {
    //if(err) throw err
    if (err) {
      res.json({
        message: 'Not save',
        description: err
      });
    } else {
      res.json({
        message: 'success',
        description: 'User successfully added'
      });

    }
  })
})



module.exports = router;
